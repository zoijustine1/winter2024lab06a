import java.util.Scanner;
public class LuckyCardGameApp {
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Deck deckOfCards = new Deck();
		deckOfCards.shuffle();
		System.out.println("How many cards do you wish to remove?");
		int userInput = reader.nextInt();
		if(userInput < 0 || userInput > 52){
			System.out.println("Invalid input. Enter a valid number");
			userInput = reader.nextInt();
		}
		System.out.println("The initial number of cards is " + deckOfCards.length());
		int cardsLeft = deckOfCards.length() - userInput;
		System.out.println("There are only " + cardsLeft + " cards left on the deck");
		System.out.println("removed " + deckOfCards.drawTopCard());
		System.out.println("removed " + deckOfCards.drawTopCard());
		System.out.println("removed " + deckOfCards.drawTopCard());
		System.out.println("removed " + deckOfCards.drawTopCard());
		System.out.println("removed " + deckOfCards.drawTopCard());
		deckOfCards.shuffle();
		System.out.println(deckOfCards);
	
		
	}
	
}